<?php

/**
 * @file
 * Contains \Drupal\test\Plugin\Block\BigPipeDemoBlock.
 */

namespace Drupal\big_pipe_demo\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use GuzzleHttp\Client;
use GuzzleHttp\RequestOptions;
use Drupal\Component\Serialization\Json;
use Exception;

/**
 * Provides a 'BigPipeDemoBlock' block.
 *
 * @Block(
 *  id = "big_pipe_demo_http_block",
 *  admin_label = @Translation("Big Pipe Demo: HTTP Service Block"),
 * )
 */
class BigPipeDemoHttpBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * GuzzleHttp\Client definition.
   *
   * @var \GuzzleHttp\Client
   */
  protected $http_client;
  /**
   * Construct.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param string $plugin_definition
   *   The plugin implementation definition.
   * @param Client $http_client
   *   The HTTP Client service.
   */
  public function __construct(
        array $configuration,
        $plugin_id,
        $plugin_definition,
        Client $http_client
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->http_client = $http_client;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('http_client')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $build['big_pipe_demo_block']['#cache'] = [
      'max-age' => 0,
    ];
    $build['big_pipe_demo_block']['start'] = [
      '#markup' => '<p>' . $this->t('Block rendering started at @time.', [
        '@time' => time(),
      ]) . '</p>',
    ];
    try {
      $response = $this->http_client->get('http://demo.ckan.org/api/3/action/package_list?limit=300', [
        RequestOptions::CONNECT_TIMEOUT => 3,
        RequestOptions::TIMEOUT => 3,
      ]);
      $data = (object) Json::decode($response->getBody());
      if ($data->success) {
        $build['big_pipe_demo_block']['response'] = [
         '#markup' => '<p>' . $this->t('Results returned from ckan service:', [
            '@time' => time(),
          ]) . '</p>',
        ];
        $build['big_pipe_demo_block']['response_list'] = [
          '#theme' => 'item_list',
          '#items' => $data->result,
          '#attributes' => [
            'style' => 'max-height: 100px; overflow: scroll;'
          ],
        ];
      }
      else {
        throw new Exception($this->t('CKAN request package_list request failed.'));
      }
    }
    catch(Exception $e) {
      $build['big_pipe_demo_block']['#markup'] = $this->t('Request failed with exception: @message.', array(
        '@message' => $e->getMessage(),
      ));
      watchdog_exception('big_pipe_demo', $e, $e->getMessage());
    }

    $build['big_pipe_demo_block']['finish'] = [
      '#markup' => '<p>' . $this->t('Block rendering finished at @time.', [
        '@time' => time(),
      ]) . '</p>',
    ];
    return $build;
  }

}
