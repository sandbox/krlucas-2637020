<?php

/**
 * @file
 * Contains \Drupal\test\Plugin\Block\BigPipeDemoBlock.
 */

namespace Drupal\big_pipe_demo\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides a 'BigPipeDemoBlock' block.
 *
 * @Block(
 *  id = "big_pipe_demo_slow_block",
 *  admin_label = @Translation("Big Pipe Demo: Slow Block"),
 * )
 */
class BigPipeDemoSlowBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $form['sleep_seconds'] = array(
      '#type' => 'select',
      '#title' => $this->t('Sleep seconds'),
      '#description' => $this->t('The number of seconds the block should rest before rendering output.'),
      '#default_value' => isset($this->configuration['sleep_seconds']) ? $this->configuration['sleep_seconds'] : '1',
      '#options' => [1 => 1, 2 => 2, 3 => 3],
      '#weight' => '10',
    );
    $form['max_age'] = array(
      '#type' => 'number',
      '#title' => $this->t('Max Age'),
      '#description' => $this->t('The cache max-age for this block. Set to 0 to completely disable caching.'),
      '#default_value' => isset($this->configuration['max_age']) ? $this->configuration['max_age'] : '0',
      '#weight' => '20',
    );
    return $form;
  }


  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    $this->configuration['sleep_seconds'] = $form_state->getValue('sleep_seconds');
    $this->configuration['max_age'] = $form_state->getValue('max_age');
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $build = [];
    $markup = '<p>' . $this->t('Rendering started: @time', array(
      '@time' => time(),
    )) . '</p>';
    $markup .= '<p>' . $this->t('Sleeping for @seconds seconds...', array(
        '@seconds' => $this->configuration['sleep_seconds'],
      )) . '</p>';
    sleep($this->configuration['sleep_seconds']);
    $markup .= '<p>' . $this->t('Rendering finished: @time', array(
      '@time' => time(),
    )) . '</p>';
    $build['big_pipe_demo_block']['#markup'] = $markup;
    $build['big_pipe_demo_block']['#cache'] = [
      'max-age' => $this->configuration['max_age'],
    ];
    return $build;
  }

}
